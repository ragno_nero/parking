﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parking
{
    struct Rate
    {
        public float Car;
        public float Truck;
        public float Bus;
        public float Motocycle;
    }
}
