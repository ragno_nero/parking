﻿namespace Parking
{
    static class Setting
    {
        public static double Balance { get; set; }
        public static int Capacity { get; set; }
        public static int PayTime { get; set; }
        public static Rate Tariff { get; set; }
        public static float Fine { get; set; }
        static Setting()
        {
            Balance = 0;
            Capacity = 10;
            PayTime = 5;
            Tariff = new Rate
            {
                Car = 2f,
                Truck = 5f,
                Bus = 3.5f,
                Motocycle = 1f
            };
            Fine = 2.5f;
        }
    }
}
