﻿using Parking.Transport;
using System;

namespace Parking
{
    class Program
    {
        static Parking parking = Parking.GetInstance();
        static Logger.Logger logger = Logger.ExceptionLogger.GetInstance();

        static void Main(string[] args)
        {

            bool menu = true;
            while (menu)
            {
                int input = EnterMenuChoice();
                switch (input)
                {
                    case 1:
                        Console.WriteLine("Баланс = " + parking.Balance);
                        break;
                    case 2:
                        Console.WriteLine("Сума зароблених коштів за останню хвилину: " + parking.Profit);
                        break;
                    case 3:
                        Console.WriteLine($"Кількість вільних місць на парковці: {Setting.Capacity - parking.Transports.Count}, зайнято: {parking.Transports.Count} місць");
                        break;
                    case 4:
                        parking.PrintTransactions();
                        break;
                    case 5:
                        parking.ReadTransactions();
                        break;
                    case 6:
                        parking.PrintTransports();
                        break;
                    case 7:
                        parking.AddTransport(CreateTransport());
                        break;
                    case 8:
                        RemoveTransport();
                        break;
                    case 9:
                        RefillTransportBalance();
                        break;
                    case 0:
                        menu = false;
                        break;
                    default:
                        Console.WriteLine("Такий пункт меню відсутній! Повторіть, будь ласка, ввід!");
                        break;
                }
                Console.ReadKey();
                Console.Clear();
            }
        }

        private static void Menu()
        {
            Console.Write("\tПрограма \'Симуляція парковки\':\n" +
                                 "1. Дізнатися баланс парковки\n" +
                                 "2. Сума зароблених коштів за останню хвилину\n" +
                                 "3. Дізнатися кількість вільних/зайнятих місць на парковці\n" +
                                 "4. Вивести на екран усі Транзакції Праковки за останню хвилину\n" +
                                 "5. Вивести усю історію Транзакцій\n" +
                                 "6. Вивести на екран список усіх Транспортних засобів\n" +
                                 "7. Створити/поставити Транспортний засіб на Парковку\n" +
                                 "8. Видалити/забрати Транспортний засіб з Парковки\n" +
                                 "9. Поповнити баланс конкретного Транспортного засобу\n" +
                                 "0. Вихід\n" +
                                 "\t Ваш вибір: ");
        }

        private static Transport.Transport CreateTransport()
        {
            int type = EnterType();
            if (type == -1) return null;
            Console.Write("Введіть баланс автомобіля: ");
            float balance = EnterBalance();
            if (balance > 0)
                switch (type)
                {
                    case 1: return new Car(balance);
                    case 2: return new Bus(balance);
                    case 3: return new Truck(balance);
                    case 4: return new Motocycle(balance);
                }
            return null;
        }

        private static void RemoveTransport()
        {
            Console.Write("Введіть Id автомобіля: ");
            int id = Int32.Parse(Console.ReadLine());
            if (id == -1) return;
            parking.RemoveTransport(id);
        }

        private static void RefillTransportBalance()
        {
            int id = EnterId();
            if (id == -1) return;
            Transport.Transport t = parking.GetTransport(id);
            if (t != null)
            {
                Console.Write("Введіть баланс автомобіля: ");
                float balance = float.Parse(Console.ReadLine());
                if (balance < 0) return;
                t.RefillBalance(balance);
                Console.WriteLine($"Баланс автомобіля {id} поповнено!");
            }
            else Console.WriteLine($"Автомобіль з Id {id} відсутній на парковці!");
        }

        private static int EnterId()
        {
            try
            {
                Console.Write("Введіть Id автомобіля: ");
                int id = Int32.Parse(Console.ReadLine());
                if (id < 0) throw new IndexOutOfRangeException("Exception: Значення знаходяться поза діапазоном допустимих значень");
                return id;
            }
            catch (IndexOutOfRangeException re)
            {
                logger.WriteLog(re.Message);
                Console.WriteLine(re.Message);
                return -1;
            }
            catch (ArgumentException ae)
            {
                logger.WriteLog(ae.Message);
                Console.WriteLine("Exception: Введено некоректне значення");
                return -1;
            }
            catch (Exception ex)
            {
                logger.WriteLog(ex.Message);
                Console.WriteLine("Exception: " + ex.Message);
                return -1;
            }
        }

        private static int EnterType()
        {
            try
            {
                Console.Write("Тип транспорту (1. Автомобіль, 2. Автобус, 3. Вантажний, 4. Мотоцикл): ");
                int type = Int32.Parse(Console.ReadLine());
                if (type < 1 || type > 4) throw new IndexOutOfRangeException("Значення знаходяться поза діапазоном допустимих значень");
                return type;
            }
            catch (IndexOutOfRangeException re)
            {
                logger.WriteLog(re.Message);
                Console.WriteLine(re.Message);
                return -1;
            }
            catch (ArgumentException ae)
            {
                logger.WriteLog(ae.Message);
                Console.WriteLine("Exception: Введено некоректне значення");
                return -1;
            }
            catch (Exception ex)
            {
                logger.WriteLog(ex.Message);
                Console.WriteLine("Exception: " + ex.Message);
                return -1;
            }
        }

        private static float EnterBalance()
        {
            try
            {
                Console.Write("Введіть баланс автомобіля: ");
                float balance = float.Parse(Console.ReadLine());
                if (balance < 0) throw new IndexOutOfRangeException("Значення знаходяться поза діапазоном допустимих значень");
                return balance;
            }
            catch (IndexOutOfRangeException re)
            {
                logger.WriteLog(re.Message);
                Console.WriteLine(re.Message);
                return -1f;
            }
            catch (ArgumentException ae)
            {
                logger.WriteLog(ae.Message);
                Console.WriteLine("Exception: Введено некоректне значення");
                return -1f;
            }
            catch (Exception ex)
            {
                logger.WriteLog(ex.Message);
                Console.WriteLine("Exception: " + ex.Message);
                return -1f;
            }
        }

        private static int EnterMenuChoice()
        {
            try
            {
                Menu();
                int choice = Int32.Parse(Console.ReadLine());
                if (choice < 0 || choice > 10) throw new IndexOutOfRangeException("Значення знаходяться поза діапазоном допустимих значень");
                return choice;
            }
            catch (IndexOutOfRangeException re)
            {
                logger.WriteLog(re.Message);
                Console.WriteLine(re.Message);
                return -1;
            }
            catch (ArgumentException ae)
            {
                logger.WriteLog(ae.Message);
                Console.WriteLine("Exception: Введено некоректне значення");
                return -1;
            }
            catch (Exception ex)
            {
                logger.WriteLog(ex.Message);
                Console.WriteLine("Exception: " + ex.Message);
                return -1;
            }
        }
    }
}
