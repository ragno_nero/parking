﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parking.Logger
{
    public class TransactionLogger : Logger
    {
        private static TransactionLogger instance;
        private TransactionLogger()
        {
            LoggerFile = "Transactions.log";
        }

        public static TransactionLogger GetInstance()
        {
            if (instance == null)
                instance = new TransactionLogger();
            return instance;
        }
        public override void WriteLog(string line)
        {
            File.AppendAllText(LoggerFile, line+Environment.NewLine);
        }
    }
}
