﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parking.Logger
{
    public class ExceptionLogger : Logger
    {
        private static ExceptionLogger instance;
        private ExceptionLogger()
        {
            LoggerFile = "Exceptions.log";
        }

        public static ExceptionLogger GetInstance()
        {
            if (instance == null)
                instance = new ExceptionLogger();
            return instance;
        }

        public override void WriteLog(string line)
        {
            File.AppendAllText(LoggerFile, $"Time: {DateTime.Now.ToString("HH:mm:ss")}, Exception: {line}{Environment.NewLine}");
        }
    }
}
