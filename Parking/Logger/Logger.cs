﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parking.Logger
{
    public abstract class Logger
    {
        public string LoggerFile { get; protected set; }
        public abstract void WriteLog(string line);
    }
}
