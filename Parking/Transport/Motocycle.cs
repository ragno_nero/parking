﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace Parking.Transport
{
    class Motocycle: Transport
    {
        private static float Fee { get; set; }
        private static string Type { get; set; }
        static Motocycle()
        {
            Fee = Setting.Tariff.Motocycle;
            Type = "Мотоцикл";
        }

        public Motocycle(float balance) : base(balance)
        { }

        public override float GetFee()
        {
            return Fee;
        }

        public override string GetType()
        {
            return Type;
        }
    }
}
