﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace Parking.Transport
{
    class Bus : Transport
    {
        private static float Fee { get; set; }
        private static string Type { get; set; }
        static Bus()
        {
            Fee = Setting.Tariff.Bus;
            Type = "Автобус";
        }

        public Bus(float balance) : base(balance)
        { }

        public override float GetFee()
        {
            return Fee;
        }

        public override string GetType()
        {
            return Type ;
        }
    }
}
