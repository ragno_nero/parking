﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace Parking.Transport
{
    class Truck : Transport
    {
        private static float Fee { get; set; }
        private static string Type { get; set; }
        static Truck()
        {
            Fee = Setting.Tariff.Truck;
            Type = "Вантажний автомобіль";
        }

        public Truck(float balance) : base(balance)
        { }

        public override float GetFee()
        {
            return Fee;
        }

        public override string GetType()
        {
            return Type;
        }
    }
}
