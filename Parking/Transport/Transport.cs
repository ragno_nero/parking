﻿using System;
using System.Timers;

namespace Parking.Transport
{
    public abstract class Transport
    {
        private static int Counter = 1;
        public int Id { get; protected set; }
        public float Balance { get; protected set; }
        public Timer timer { get; private set; }  

        protected Transport(float balance)
        {
            Id = Counter;
            Balance = balance;
            ++Counter;
            timer = new Timer(Setting.PayTime * 1000);
            timer.Enabled = true;
            timer.AutoReset = true;
            timer.Elapsed += Paid;
        }
      
        protected virtual void Paid(object sender, ElapsedEventArgs e)
        {
            float fee = GetFee();
            if (Balance > fee)
            {
                Balance -= fee;
                CreateTransaction(Id, fee);
                PaidToParking(fee);
            }
            else
            {
                Balance -= fee * Setting.Fine;
                PaidToParking(fee * Setting.Fine);
            }
        }

        public virtual void AddMoney(float balance)
        {
            Balance += balance;
        }

        protected virtual void CreateTransaction(int Id, float paid)
        {
            Parking.GetInstance().Transactions.Add(new Transaction(Id, paid));
        }

        public override string ToString()
        {
            return $"Тип: {GetType()}, Id: {Id}, Баланс: {Balance}";
        }

        public virtual void RefillBalance(float balance)
        {
            Balance += balance;
        }

        public virtual void PaidToParking(float fee)
        {
            Parking.GetInstance().AddParkingBalance(fee);
        }

        public abstract string GetType();
        public abstract float GetFee();
    }
}
