﻿using Parking.Logger;
using System;
using System.Timers;

namespace Parking
{
    public class Transaction
    {
        private static int Count;
        public int Id { get; set; }
        public DateTime Time { get; set; }
        public int CarId { get; set; }
        public float MoneyAmount { get; set; }

        static Logger.Logger Logger;

        static Transaction ()
        {
            Count = 1;
            Logger = TransactionLogger.GetInstance();
        }

        public Transaction(int car, float money)
        {
            Id = Count;
            Time = DateTime.Now;
            CarId = car;
            MoneyAmount = money;
            ++Count;
        }

        public override string ToString()
        {
            return $"Id: {Id}, Time: {Time.ToString("HH:mm:ss")}, CarId: {CarId}, MoneyAmount: {MoneyAmount}";
        }
    }
}
