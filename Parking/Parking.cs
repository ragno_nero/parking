﻿using Parking.Logger;
using System;
using System.Collections.Generic;
using System.Timers;
using System.Linq;
using System.IO;

namespace Parking
{
    class Parking
    {
        private static Parking instance;
        public double Balance { get; private set; }
        public List<Transaction> Transactions { get; private set; }
        public List<Transport.Transport> Transports { get; private set; }
        private static Logger.Logger TransactionLogger { get; set; }
        private static Timer TransactionTimer { get; set; }
        private static TimeSpan Span { get; set; }
        public double Profit { get; private set; }

        private Parking()
        {
            Balance = Setting.Balance;
            TransactionLogger = Logger.TransactionLogger.GetInstance();
            Span = new TimeSpan(0, 1, 0);
            TransactionTimer = new Timer(60000);
            TransactionTimer.Elapsed += ClearData;
            TransactionTimer.AutoReset = true;
            TransactionTimer.Enabled = true;
            Transports = new List<Transport.Transport>();
            Transactions = new List<Transaction>();
        }

        private void ClearData(object sender, ElapsedEventArgs e)
        {
            foreach (var item in Transactions)
            {
                TransactionLogger.WriteLog(item.ToString());
            }
            Transactions.Clear();
            Profit = 0;
        }

        public static Parking GetInstance()
        {
            if (instance == null)
                instance = new Parking();
            return instance;
        }

        public void AddTransport(Transport.Transport car)
        {
            if (car == null) return;
            if (Setting.Capacity != Transports.Count)
                try
                {
                    Transports.Add(car);
                    Transactions.Add(new Transaction(car.Id, car.GetFee()));
                    Balance += car.GetFee();
                    Profit += car.GetFee();
                    Console.WriteLine($"Автомобіль з Id: {car.Id} поставлено на парковку");
                    return;
                }
                catch (Exception ex)
                {
                    ExceptionLogger.GetInstance().WriteLog(ex.Message);
                    Console.WriteLine("Exception: " + ex.Message);
                    return;
                }
            else
                Console.WriteLine("Парковка переповнена!");
        }

        public void RemoveTransport(int id)
        {
            try
            {
                Transport.Transport t = GetTransport(id);
                t.timer.Enabled = false;
                if (t.Balance < 0)
                {
                    Console.WriteLine("Щоб забрати автомобіль з паркінгу, поповніть, будь ласка, баланс! Теперішній баланс: " + t.Balance);
                    return;
                }
                Transports.Remove(t);
                Console.WriteLine($"Автомобіль з {id} покинув парковку");
            }
            catch (NullReferenceException)
            {
                ExceptionLogger.GetInstance().WriteLog("Такий транспортний засіб відсутній!");
                Console.WriteLine("Такий транспортний засіб відсутній!");
                return;
            }
            catch (Exception ex)
            {
                ExceptionLogger.GetInstance().WriteLog(ex.Message);
                Console.WriteLine($"Exception: автомобіль з {id} відсутній на парковці!");
                return;
            }
        }

        public Transport.Transport GetTransport(int id)
        {
            try
            {
                return Transports.Where(tr => tr.Id == id).First();
            }
            catch (NullReferenceException)
            {
                ExceptionLogger.GetInstance().WriteLog("Такий транспортний засіб відсутній!");
                Console.WriteLine("Такий транспортний засіб відсутній!");
                return null;
            }
            catch (Exception ex)
            {
                ExceptionLogger.GetInstance().WriteLog(ex.Message);
                Console.WriteLine("Exception: " + ex.Message);
                return null;
            }
        }

        public void ReadTransactions()
        {
            try
            {
                var lines = File.ReadAllLines(TransactionLogger.LoggerFile);
                if (lines.Length > 0)
                {
                    Console.WriteLine("Транзакції: ");
                    foreach (var l in lines)
                        Console.WriteLine("\t" + l);
                }
                else Console.WriteLine("Транзації відсутні!");
            }
            catch (IOException)
            {
                ExceptionLogger.GetInstance().WriteLog("Відсутній файл");
                Console.WriteLine("Відсутній файл");
                return;
            }
            catch (Exception ex)
            {
                ExceptionLogger.GetInstance().WriteLog(ex.Message);
                Console.WriteLine("Exception: " + ex.Message);
                return;
            }
        }

        public void AddParkingBalance(float fee)
        {
            Balance += fee;
            Profit += fee;
        }

        public void PrintTransactions()
        {
            if (Transactions.Count > 0)
            {
                Console.WriteLine("Транзакції за останню хвилину:");
                foreach (var t in Transactions)
                    Console.WriteLine("\t" + t);
            }
            else Console.WriteLine("Транзації відсутні!");
        }
        public void PrintTransports()
        {
            if (Transports.Count > 0)
            {
                Console.WriteLine("Транспортні засоби: ");
                foreach (var t in Transports)
                    Console.WriteLine("\t" + t);
            }
            else Console.WriteLine("Транспортні засоби відсутні!");
        }
    }
}
